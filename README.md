# KIV-PSI-4-NAT

ŘešenÍ
- CISCO IOS

IP Rozsahy:

R2 (Domácí síť) (požadavek 20 hostů -> má 30 (32 adres))
- 192.168.123.0 /27
- 192.168.123.0 - 192.168.123.31

R1 <--> R2 (Mezisíť) (Požadavek 2 hosty -> má 2 (4 adresy))
- 192.168.123.32 /30
- 192.168.123.32 - 192.168.123.35

HELP (http://jodies.de/ipcalc)

# Konfigurace

## R1 (config)
- enable
- configure terminal
- ip domain-lookup
- ip name-server 8.8.4.4
- ip nat inside source list 1 interface fa1/0 overload
- access-list 1 permit any

### Síť s R2
- int fa0/0
- ip addr 192.168.123.33 255.255.255.252
- ip nat inside
- no shutdown
- exit

### Směrování
- router rip
- version 2
- network 192.168.123.0
- exit

### Ven
- int fa1/0
- ip address dhcp
- ip nat outside
- no shutdown
- exit


## R2 (config)
- enable
- configure terminal
- ip domain-lookup
- ip name-server 8.8.4.4

### Síť s R1
- int fa0/0
- ip addr 192.168.123.34 255.255.255.252
- no shutdown
- exit

### Směrování
- router rip
- version 2
- network 192.168.123.0
- exit

### Domácí síť
- int fa1/0
- ip addr 192.168.123.1 255.255.255.224
- no shutdown
- exit


## PC1
- IP 192.168.123.10
- MASK 255.255.255.224
- DGW 192.168.123.1
- DNS1 8.8.4.4

## PC2
- IP 192.168.123.11
- MASK 255.255.255.224
- DGW 192.168.123.1
- DNS1 8.8.4.4
